package pl.jdudycz.eci.engine.job

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import pl.jdudycz.eci.common.domain.Repo.*
import pl.jdudycz.eci.common.domain.event.jobCreate
import pl.jdudycz.eci.common.kafka.core.EventConsumer
import pl.jdudycz.eci.common.kafka.core.EventPublisher
import pl.jdudycz.eci.common.util.logger
import javax.annotation.PostConstruct

@Service
class JobListener(@Qualifier("repoChanged") private val consumer: EventConsumer,
                  private val jobCreator: JobCreator,
                  private val publisher: EventPublisher
) {

    @PostConstruct
    fun init() {
        consumer.consume()
                .map { ChangeData.parseFrom(it.data) }
                .doOnNext { log.debug("${it.repoUrl}/${it.refValue} has been updated") }
                .flatMap(jobCreator::createJobs)
                .map(::jobCreate)
                .transform(publisher::publish)
                .subscribe()
    }

    companion object {
        private val log = logger<JobListener>()
    }
}
