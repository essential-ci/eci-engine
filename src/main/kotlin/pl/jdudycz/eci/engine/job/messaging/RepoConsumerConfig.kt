package pl.jdudycz.eci.engine.job.messaging

import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import pl.jdudycz.eci.common.kafka.core.EventConsumer
import pl.jdudycz.eci.common.kafka.REPO_CHANGED_TOPIC

@Configuration
class RepoConsumerConfig(val kafkaProperties: KafkaProperties) {

    @Bean
    fun repoChanged(): EventConsumer = EventConsumer.create(REPO_CHANGED_TOPIC, kafkaProperties)
}
