package pl.jdudycz.eci.engine.job.persistence

import org.springframework.data.mongodb.core.mapping.Document
import pl.jdudycz.eci.common.domain.Project

@Document
data class Project(
        val id: String,
        val spaceId: String,
        val name: String,
        val repoUrl: String,
        val token: String?,
        val variables: Map<String, Any>,
        val secretVariables: Map<String, Any>
) {

    fun toProjectData(): Project.ProjectData =
            Project.ProjectData.newBuilder()
                    .setId(id)
                    .setSpaceId(spaceId)
                    .setName(name)
                    .setRepoUrl(repoUrl)
                    .setIsPrivate(token != null)
                    .setToken(token)
                    .build()
}
