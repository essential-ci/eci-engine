package pl.jdudycz.eci.engine.job

import org.springframework.stereotype.Service
import pl.jdudycz.eci.common.domain.Job
import pl.jdudycz.eci.common.domain.Repo
import pl.jdudycz.eci.common.util.doOnEmpty
import pl.jdudycz.eci.common.util.logger
import pl.jdudycz.eci.common.util.resumeOnError
import pl.jdudycz.eci.engine.job.persistence.Project
import pl.jdudycz.eci.engine.job.persistence.ProjectRepository
import pl.jdudycz.eci.engine.pipeline.PipelineReader
import pl.jdudycz.eci.engine.pipeline.model.CheckoutData
import pl.jdudycz.eci.engine.pipeline.model.Task
import pl.jdudycz.eci.engine.pipeline.model.TriggerType
import reactor.core.publisher.Flux

@Service
class JobCreator(private val pipelineReader: PipelineReader,
                 private val projectRepository: ProjectRepository) {

    fun createJobs(change: Repo.ChangeData): Flux<Job.JobData> =
            projectRepository
                    .findByRepoUrl(change.repoUrl)
                    .doOnEmpty { log.debug("Project with repo ${change.repoUrl} not found - skipping...") }
                    .flatMap { createJob(it, change) }

    private fun createJob(project: Project, change: Repo.ChangeData) =
            pipelineReader
                    .readTasks(CheckoutData(change, project.token), TriggerType.valueOf(change.type))
                    .filter { it.isNotEmpty() }
                    .doOnEmpty { log.debug("Skipping job for project ${project.id} - no tasks to run") }
                    .doOnNext { log.debug("Parsed ${it.size} tasks for ${project.id}") }
                    .map { enrichTaskVariables(it, project, createRefEnv(change.refValue)) }
                    .map { createJobRequest(project, change, it) }
                    .doOnNext { log.debug("Job for project ${it.project.id} created") }
                    .resumeOnError { log.debug("Failed to create job for project ${project.id}: ${it.localizedMessage}") }

    private fun createRefEnv(refValue: String): Pair<String, String> =
            REF_ENV_KEY to refValue.split("/").last()

    private fun enrichTaskVariables(tasks: List<Task>,
                                    project: Project,
                                    refEnv: Pair<String, String>) =
            tasks.map {
                it.copy(
                        variables = mapOf(refEnv) + project.variables + it.variables,
                        secretVariables = project.secretVariables
                )
            }

    private fun createJobRequest(project: Project,
                                 change: Repo.ChangeData,
                                 tasks: List<Task>): Job.JobData =
            Job.JobData
                    .newBuilder()
                    .setProject(project.toProjectData())
                    .setChange(change)
                    .addAllTasks(tasks.map(Task::toPipelineTask))
                    .build()

    companion object {
        private const val REF_ENV_KEY = "REF_VALUE"
        private val log = logger<JobCreator>()

    }
}
