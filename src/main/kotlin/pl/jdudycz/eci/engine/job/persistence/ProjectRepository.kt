package pl.jdudycz.eci.engine.job.persistence

import org.springframework.data.repository.reactive.ReactiveCrudRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux

@Repository
interface ProjectRepository : ReactiveCrudRepository<Project, String> {

    fun findByRepoUrl(repoUrl: String): Flux<Project>
}
