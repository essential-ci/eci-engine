package pl.jdudycz.eci.engine.pipeline.model

import com.fasterxml.jackson.annotation.JsonProperty
import pl.jdudycz.eci.common.domain.Repo

enum class TriggerType {
    @JsonProperty("branch")
    BRANCH,

    @JsonProperty("tag")
    TAG;

    companion object {
        fun valueOf(triggerType: Repo.TriggerType) = valueOf(triggerType.toString())
    }
}
