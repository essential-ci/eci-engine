package pl.jdudycz.eci.engine.pipeline.model

import pl.jdudycz.eci.common.domain.Job
import pl.jdudycz.eci.common.util.asEnvString

data class Task(val ordinal: Int,
                val name: String,
                val image: String,
                val variables: Map<String, Any>,
                val secretVariables: Map<String, Any>,
                val commands: List<String>) {

    fun toPipelineTask(): Job.PipelineTask =
            Job.PipelineTask.newBuilder()
                    .setOrdinal(ordinal)
                    .setName(name)
                    .setImage(image)
                    .addAllVariables(variables.asEnvString())
                    .addAllSecretVariables(secretVariables.asEnvString())
                    .addAllCommands(commands)
                    .build()
}
