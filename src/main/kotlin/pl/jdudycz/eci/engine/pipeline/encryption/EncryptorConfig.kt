package pl.jdudycz.eci.engine.pipeline.encryption

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import pl.jdudycz.eci.common.util.encryption.TextEncryptor

@Configuration
class EncryptorConfig {

    @Bean
    fun textEncryptor(@Value("\${pl.jdudycz.eci.engine.encryption.password}")
                      password: String,
                      @Value("\${pl.jdudycz.eci.engine.encryption.salt}")
                      salt: String): TextEncryptor = TextEncryptor(password, salt)
}
