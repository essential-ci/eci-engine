package pl.jdudycz.eci.engine.pipeline

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import org.springframework.stereotype.Service
import pl.jdudycz.eci.common.util.logger
import pl.jdudycz.eci.engine.pipeline.model.PipelineSpec
import pl.jdudycz.eci.engine.pipeline.model.Task
import pl.jdudycz.eci.engine.pipeline.model.TaskSpec
import pl.jdudycz.eci.engine.pipeline.model.TriggerType
import reactor.core.publisher.Mono
import java.io.File


@Service
class PipelineParser {

    private val mapper = ObjectMapper(YAMLFactory()).also {
        it.registerModule(KotlinModule())
    }

    fun parseScript(specFile: File, triggerType: TriggerType): Mono<List<Task>> = Mono.fromCallable {
        if (!specFile.exists()) emptyList<TaskSpec>()
        val spec = mapper.readValue<PipelineSpec>(specFile)
        if (spec.tasks.isEmpty()) emptyList<TaskSpec>()
        val globalEnvs = spec.variables ?: emptyMap()
        spec.tasks
                .filter { shouldBeRan(spec.triggers, it, triggerType) }
                .mapIndexed { index, task -> parseTask(index, task, globalEnvs) }
    }

    private fun shouldBeRan(specGlobalTriggers: List<TriggerType>, task: TaskSpec, triggerType: TriggerType): Boolean {
        return if (task.triggers == null) specGlobalTriggers.contains(triggerType)
        else task.triggers.contains(triggerType)
    }

    private fun parseTask(ordinal: Int, spec: TaskSpec, globalVars: Map<String, Any>): Task {
        if (spec.command == null && spec.commands == null)
            throw Exception("No commands specified")
        if (spec.command != null && spec.commands != null)
            throw Exception("\"command\" and \"commands\" options are exclusive")

        val commandsList = spec.commands ?: listOf(spec.command!!)
        val taskEnvs = spec.variables ?: emptyMap()
        return Task(ordinal, spec.name, spec.image, globalVars + taskEnvs, emptyMap(), commandsList)
    }
}
