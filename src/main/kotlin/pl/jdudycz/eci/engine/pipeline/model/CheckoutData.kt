package pl.jdudycz.eci.engine.pipeline.model

import pl.jdudycz.eci.common.domain.Repo

data class CheckoutData(val repoUrl: String,
                        val pullSha: String,
                        val ref: String,
                        val token: String?) {

    constructor(change: Repo.ChangeData, token: String?) : this(
            change.repoUrl,
            change.checkoutSha,
            change.refValue,
            token
    )
}
