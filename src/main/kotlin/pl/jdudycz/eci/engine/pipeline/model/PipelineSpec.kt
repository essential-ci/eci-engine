package pl.jdudycz.eci.engine.pipeline.model

data class PipelineSpec(val triggers: List<TriggerType>, val variables: Map<String, Any>?, val tasks: List<TaskSpec>)
