package pl.jdudycz.eci.engine.pipeline.model

data class TaskSpec(val name: String,
                    val triggers: List<TriggerType>?,
                    val image: String,
                    val variables: Map<String, Any>?,
                    val commands: List<String>?,
                    val command: String?)
