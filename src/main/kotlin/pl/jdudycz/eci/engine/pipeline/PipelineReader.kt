package pl.jdudycz.eci.engine.pipeline


import com.fasterxml.uuid.Generators
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider
import org.springframework.stereotype.Service
import pl.jdudycz.eci.common.util.encryption.TextEncryptor
import pl.jdudycz.eci.common.util.logger
import pl.jdudycz.eci.common.util.resumeOnError
import pl.jdudycz.eci.engine.pipeline.model.CheckoutData
import pl.jdudycz.eci.engine.pipeline.model.Task
import pl.jdudycz.eci.engine.pipeline.model.TriggerType
import reactor.core.publisher.Mono
import java.io.File

@Service
class PipelineReader(private val interpreter: PipelineParser,
                     private val textEncryptor: TextEncryptor) {

    fun readTasks(checkoutData: CheckoutData, triggerType: TriggerType): Mono<List<Task>> = checkoutData.run {
        val localRepoDir = File("$CHECKOUT_DIR/${idGenerator.generate()}")
        checkout(checkoutData, localRepoDir)
                .doOnNext { log.debug("Repository ${checkoutData.repoUrl} is accessible. Proceeding..") }
                .doOnError { log.debug("Failed to checkout repo $repoUrl, sha $pullSha: ${it.localizedMessage}") }
                .flatMap { readTasks(it, triggerType) }
    }

    private fun checkout(checkoutData: CheckoutData, localDir: File): Mono<File> = Mono.fromCallable {
        val gitRepo = Git.init().setDirectory(localDir).call()
        val fetchRequest = gitRepo.fetch()
                .setRemote(checkoutData.repoUrl)
                .setRefSpecs(checkoutData.ref)
        checkoutData.token?.let(textEncryptor::decrypt)?.also {
            fetchRequest.setCredentialsProvider(
                    UsernamePasswordCredentialsProvider(USERNAME_PLACEHOLDER, it)
            )
        }
        fetchRequest.call()
        gitRepo.checkout()
                .setStartPoint(checkoutData.pullSha)
                .addPath(PIPELINE_FILE_NAME)
                .call()
                .let { File("${localDir.absolutePath}/$PIPELINE_FILE_NAME") }
    }


    private fun readTasks(pipelineSpecFile: File, triggerType: TriggerType): Mono<List<Task>> =
            interpreter
                    .parseScript(pipelineSpecFile, triggerType)
                    .doOnError { log.debug("Failed to parse pipeline file: ${it.localizedMessage}") }
                    .doOnEach { pipelineSpecFile.parentFile.deleteRecursively() }

    companion object {
        private val log = logger<PipelineReader>()
        private val idGenerator = Generators.timeBasedGenerator()
        private const val CHECKOUT_DIR = "/tmp/eci-engine/clone"
        private const val PIPELINE_FILE_NAME = "eci-pipeline.yml"
        private const val USERNAME_PLACEHOLDER = "placeholder"
    }
}
